# Fairwork

FairWork came up as a solution for __Thomson Reuters__ challenges in __Hack'n'Lead__ 
Hackathon in Zürich in October 2019. The challenge was to use technology to fight
modern slavery in supply chains.

The solution - __FairWork__. It's a Glassdoor-like platform where migrant workers can
anonymously review their employers (not necessarily official), and share their 
experiences about work conditions, safety, compensation & report abuse.

More context here: ```https://docs.google.com/presentation/d/1qugcglbqrKBfOE60xnhW7bhF7jmW-QtI8ubu57AwXZE/```

## Backend:

Technologies used have been __Docker + Docker-Compose + DjangoREST + Postgres + NginX__

To better understand the DB & backend logic, start with DB schema (```docu/db-schema.png```).
DB has 3 tables: __Review, Employer and UserProfile__, plus standard Django table __User__.


## Running:

Current solution has been dockerised, however not deployed to a web server yet, so
needs to be run locally. To do this, perform the following steps (need to have
__Docker__ installed):

1) If you don't have Docker, download & install it first.
2) Run docker (via UI or terminal)
3) Navigate to project top folder FairWork, and type ```docker-compose up```. This runs
three containers: __DB, App & NginX__
4) Enter App container: ```docker exe -it fairwork_app_1 bash```. Now you're in the app
5) Create admin user: ```docker manage.py createsuperuser```, then type ```admin``` for
username, leave email blank, and ```admin``` for password, the confirm password, and 
choose ```y``` in the last prompt. Now you have an admin user
6) In your browser, go to ```0.0.0.0:8000/admin/``` and log in as admin
7) From here you'll be able to populate the DB via UI

## API:

To see the configured API endpoints, open file ```fairwork/app/app/api/urls.py```
To see endpoint request examples, import ```docu/FairWork.postman_collection.json```

## Contributors

The idea concept, database design and full backend has been produced by Ignas Mociunas.
Frontend (separate repo) has been done by Dieter Jackson & Saraswathi Anbu